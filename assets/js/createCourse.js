// console.log("test")
const token = localStorage.getItem('token')
const createForm = document.getElementById('createCourse')

createForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let cn = document.getElementById('courseName').value
	let desc = document.getElementById('description').value
	let price = document.getElementById('price').value

	//send a request to the server to store/save the new course
	fetch(`http://localhost:3007/api/courses/create`, {
		method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				courseName: cn,
				description: desc,
				price: price
			})
	})

	.then(result => result.json())
	.then(result => {
		console.log(result)

		if(result){
			alert(`Course successfully created!`)

			window.location.replace('./courses.html')
		}
		else{
			alert(`Please try again!`)
		}
	})
})