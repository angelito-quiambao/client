const loginForm = document.getElementById('login')

loginForm.addEventListener('submit', (e) => {
	e.preventDefault()

	//get the input of the user
	let email = document.getElementById('email').value
	let pw = document.getElementById('password').value
	let cpw = document.getElementById('cpw').value

	if(pw === cpw){
		//send the request (including the user input) to the server
		fetch(`http://localhost:3007/api/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
						email: email,
						password: pw
					})
		})

		// wait for the server's response
		.then(result => result.json())
		.then(result => {
			console.log(result)

			if(result){
				// once token is received, store it in the local storage
				localStorage.setItem('token', result.token)

				
				//request for user information & store admin and id in the local storage
				let token = localStorage.getItem('token')
				fetch(`http://localhost:3007/api/users/profile`, {
					method: "GET",
					headers:{
						"Authorization": `Bearer ${token}`
					}
				})

				//wait for server's response
				.then(result => result.json())
				.then(result => {
					// console.log(result)

					//store id & isAdmin to local storage
					localStorage.setItem('id', result._id)
					localStorage.setItem('isAdmin', result.isAdmin)

					//notify the user successful login
					alert('Login successfully!')
					
					// redirect to course page
					window.location.replace('./courses.html')
				})

			}
			else{
				alert('something went wrong. Please try again.')
			}
		})
	}
	else{
		alert('password did not match. Please try again!')
	}

})