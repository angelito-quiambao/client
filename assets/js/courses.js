const token = localStorage.getItem('token')
const isAdmin = localStorage.getItem('isAdmin')
const courseContainer = document.getElementById('anyContainer')
const adminContainer = document.getElementById('adminButton')

let courses;
let adminButton;
let cardFooter;

// console.log(typeof isAdmin)

if(isAdmin == "false"){
	//regular user
	
	//send request to get all active courses
	fetch(`http://localhost:3007/api/courses/isActive`, {
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	})

	// wait for server's response
	.then(result => result.json())
	.then(result => {
		// console.log(result)
		
		//display the courses using card
		if(result.length < 1){
			return alert("No Courses available!")
		}
		else{
			//if array is not empty, use map method to return a card for each element
			courses = result.map(course => {
				// console.log(course)
				const {courseName, description, price} = course

				return(
					`
						<div class="col-12 col-md-4 my-2">
							<div class="card" style="width: 18rem;">
								<div class="card-body">
									<h5 class="card-title">
										${courseName}
									</h5>
									<p class"card-text text-left">
										${description}
									</p>
									<p class"card-text text-right">
										${price}
									</p>
									<a class="btn btn-info" href="./specificCourse.html?courseId=">
										Select Course
									</a>
								</div>
							</div>
						</div>
					`
				)
			}).join(" ")

			console.log(courses)

			courseContainer.innerHTML = courses
		}
	})
}
else{
	//admin

	//show all the course in the courses page
		//both offered and not offered
	fetch(`http://localhost:3007/api/courses/`, {
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	})

	// wait for server's response
	.then(result => result.json())
	.then(result => {
		//display the courses using card
		if(result.length < 1){
			return alert("No Courses available!")
		}
		else{
			courses = result.map(course => {
				// console.log(course)
				const {courseName, description, price, isOffered, _id} = course

				if(isOffered){
					//if course is offered
					cardFooter =
					`
						<a class="btn btn-info btn-block" href="./editCourse.html?courseId=${_id}">
							Edit Course
						</a>
						<a class="btn btn-danger btn-block" href="./archiveCourse.html?courseId=${_id}">
							Archive
						</a>
						<a class="btn btn-secondary btn-block" href="./deleteCourse.html?courseId=${_id}">
							Delete
						</a>

					`
				}
				else{
					//if course is not offered
					cardFooter =
					`
						<a class="btn btn-success btn-block" href="./unarchiveCourse.html?courseId=${_id}">
							Unarchive
						</a>
						<a class="btn btn-secondary btn-block" href="./deleteCourse.html?courseId=${_id}">
							Delete
						</a>

					`
				}

				return(
					`
						<div class="col-12 col-md-4 my-2">
							<div class="card" style="width: 18rem;">
								<div class="card-body">
									<h5 class="card-title">
										${courseName}
									</h5>
									<p class"card-text text-left">
										${description}
									</p>
									<p class"card-text text-right">
										${price}
									</p>
									<div class="card-footer">
										${cardFooter}
									</div>
								</div>
							</div>
						</div>
							
						`
				)
			}).join(" ")

			// show the button if it is an admin

			adminButton = (`
					<a class="btn btn-info m-3" href="./createCourse.html">Add Course</a>
					<a class="btn btn-info  m-3" href="./dashboard.html">Dashboard</a>
			`)

			// console.log(courses)

			courseContainer.innerHTML = courses
			adminContainer.innerHTML = adminButton

		}
	})

	// console.log("admin")
}